# Benzinga Earnings Widget

Small widget built with [Chart.js](https://www.chartjs.org/) and vanilla JavaScript.

![Earnings Widget](widget.png)

## Run Instructions

1. Install https-server: `npm install --global http-server`
2. Run it with cors and no-cache flags: `http-server -c-1 --cors`
3. Access it at `http://127.0.0.1:8080/`
