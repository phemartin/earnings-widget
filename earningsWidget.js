// Fetch TSLA earnings
// You can use this function to fetch the API
async function getData() {
  const getData = await fetch("tsla_earnings.json");
  const data = await getData.json();
  return data;
}

// This function loads the widget in the targeted canvas
function loadEarningsWidget(data, isMobile = false) {
  // Get the widget canvas
  const context = document.getElementById("earningsWidget").getContext("2d");

  // Prepare data
  const epsData = [];
  const epsEstData = [];
  // Array to change each point color
  // https://github.com/chartjs/Chart.js/issues/2670
  const pointBackground = [];
  for (let i = 0; i < data.length; i++) {
    let eps = Number(data[i].eps);
    let epsEst = Number(data[i].eps_est);
    if (eps) {
      epsData.push({
        x: 5 - i,
        y: data[i].eps,
      });
      epsEstData.push({
        x: 5 - i,
        y: data[i].eps_est,
      });
      // Red if negative, Green otherwise
      if (eps < epsEst) {
        pointBackground.push("#b55f6c");
      } else {
        pointBackground.push("#2bb45b");
      }
    } else {
      // eps_est is empty for future data, so I used eps_prior data here
      epsEstData.push({
        x: 5 - i,
        y: data[i].eps_prior,
      });
    }
  }

  // Configure chart and styles
  const config = {
    type: "scatter",
    defaultFontFamily: (Chart.defaults.global.defaultFontFamily =
      "'Open Sans',arial,helvetica,sans-serif"),
    data: {
      datasets: [
        {
          label: "Actual",
          borderColor: "#2bb45b",
          backgroundColor: "#2bb45b",
          data: epsData,
          pointRadius: 10,
          pointHoverRadius: 8,
          pointBorderColor: [...pointBackground],
          pointBackgroundColor: [...pointBackground],
        },
        {
          label: "Estimate",
          borderColor: "#2ca2d1",
          backgroundColor: "transparent",
          data: epsEstData,
          pointRadius: 10,
          pointHoverRadius: 8,
        },
      ],
    },
    options: {
      responsive: isMobile ? true : false,
      title: {
        position: "top",
        display: true,
        text: `${data[0].name} Earnings`,
        fontSize: 24,
        fontColor: "#333",
        fontFamily:
          "'Open Sans Condensed','Open Sans',arial,helvetica,sans-serif",
      },
      legend: {
        position: "bottom",
        align: "center",
        labels: {
          boxWidth: 20,
        },
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            // eg. EPS Estimate: $ 19.30
            const label = ` ${
              data.datasets[tooltipItem.datasetIndex].label
            }: $ ${Math.round(tooltipItem.yLabel * 100) / 100}`;
            return label;
          },
        },
      },
      scales: {
        xAxes: [
          {
            display: true,
            gridLines: {
              display: false,
            },
            ticks: {
              callback: function (value, _index, _values) {
                // Get label according to index (eg. Q3 2020)
                const point = data[5 - value];
                if (!point) return "";
                return `${point.period} ${point.period_year}`;
              },
              min: 0.8, //0.2 gap as a workaround to apply offset
              max: 5,
              stepSize: 1,
            },
          },
        ],
        yAxes: [
          {
            display: true,
            ticks: {
              callback: function (value, _index, _values) {
                // Turn num into 2 digit pad string eg. 1.2 = "$ 1.20"
                const valueStr = String(value);
                const splitStr = valueStr.split(".");
                if (!splitStr[1]) splitStr[1] = "00";
                else if (splitStr[1].length == 1) splitStr[1] += "0";

                return `$ ${splitStr.join(".")}`;
              },
            },
          },
        ],
      },
    },
    plugins: [
      {
        beforeInit: function (chart, _options) {
          // Add margin beneath the legend
          chart.legend.afterFit = function () {
            this.height = this.height + 15;
          };
        },
      },
    ],
  };
  // Plot widget
  const earningsWidget = new Chart(context, config);
}

// Load widget after content is loaded (it needs to target canvas)
document.addEventListener("DOMContentLoaded", async function () {
  // Make chart responsive if it's on mobile
  const isMobile = window.matchMedia("(max-width: 420px)").matches;
  const data = await getData();
  loadEarningsWidget(data, isMobile);
});
